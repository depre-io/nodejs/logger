(function () {
  'use strict';
  require('dotenv').config()
  const winston = require('winston');
  const assign = require('object-assign');


  const defaults = {
    console: {
      level: process.env.CONSOLE_LEVEL || 'debug',
      handleExceptions: true,
      json: false,
      colorize: process.env.CONSOLE_COLORIZE || true,
      format: winston.format.combine(
        winston.format.splat(),
        winston.format.simple()
      ),
    },
  };

  let _logger;

  function wrapper(options) {
    //logger is a singleton;
    if (_logger) return _logger;

    // if options are static (either via defaults or custom options passed in), wrap in a function
    let optionsCallback = null;
    if (typeof options === 'function') {
      optionsCallback = options;
    }
    else {
      optionsCallback = function (cb) {
        cb(options);
      };
    }

    optionsCallback(function (options) {
      const o = assign({}, defaults, options);
      let transports = [new winston.transports.Console(o.console)];
      if (process.env.LOG_FILE) {
        o.file = {
          level: process.env.LOG_FILE_LEVEL || 'info',
          filename: process.env.LOG_FILE,
          handleExceptions: true,
          json: process.env.LOG_FILE_JSON || true,
          maxsize: process.env.LOG_FILE_SIZE || 10485760,
          maxFiles: process.env.LOG_FILE_MAX_FILES || 5,
          colorize: process.env.LOG_FILE_COLORIZE || false,
          format: winston.format.combine(
            winston.format.splat(),
            winston.format.json()
          ),
        }
        transports.push(new winston.transports.File(o.file));
      }

      _logger = winston.createLogger({
        transports: transports,
        exitOnError: false, // do not exit on handled exceptions
      });

      _logger.stream = {
        write: function (message, encoding) {
          _logger.info(message);
        },
      };
    });
    return _logger;

  }
  module.exports = wrapper;
}());
